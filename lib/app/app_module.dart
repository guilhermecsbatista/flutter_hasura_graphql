import 'package:flutter_hasura_graphql/app/shared/widgets/custom_combobox/custom_combobox_controller.dart';
import 'package:flutter_hasura_graphql/app/shared/widgets/label/label_controller.dart';
import 'package:flutter_hasura_graphql/app/app_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hasura_graphql/app/app_widget.dart';
import 'package:flutter_hasura_graphql/app/modules/home/home_module.dart';
import 'package:hasura_connect/hasura_connect.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => CustomComboboxController()),
        Bind((i) => LabelController()),
        Bind((i) => AppController()),
        //Outros
        Bind((i) => HasuraConnect("https://flutter-hasura-graphql-gitlab.herokuapp.com/v1/graphql")),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, module: HomeModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
