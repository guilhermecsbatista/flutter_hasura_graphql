// To parse this JSON data, do
//
//     final produtoTipoCategoriaDto = produtoTipoCategoriaDtoFromMap(jsonString);

import 'dart:convert';

class ProdutoTipoCategoriaDto {
    ProdutoTipoCategoriaDto({
        this.tipoProduto,
        this.categoriaProduto,
        this.produtoByPk,
    });

    final List<TipoCategoriaDto> tipoProduto;
    final List<TipoCategoriaDto> categoriaProduto;
    final Produto produtoByPk;

    ProdutoTipoCategoriaDto copyWith({
        List<TipoCategoriaDto> tipoProduto,
        List<TipoCategoriaDto> categoriaProduto,
        Produto produtoByPk,
    }) => 
        ProdutoTipoCategoriaDto(
            tipoProduto: tipoProduto ?? this.tipoProduto,
            categoriaProduto: categoriaProduto ?? this.categoriaProduto,
            produtoByPk: produtoByPk ?? this.produtoByPk,
        );

    factory ProdutoTipoCategoriaDto.fromJson(String str) => ProdutoTipoCategoriaDto.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory ProdutoTipoCategoriaDto.fromMap(Map<String, dynamic> json) => ProdutoTipoCategoriaDto(
        tipoProduto: List<TipoCategoriaDto>.from(json["tipo_produto"].map((x) => TipoCategoriaDto.fromMap(x))),
        categoriaProduto: List<TipoCategoriaDto>.from(json["categoria_produto"].map((x) => TipoCategoriaDto.fromMap(x))),
        produtoByPk: Produto.fromMap(json["produto_by_pk"]),
    );

    Map<String, dynamic> toMap() => {
        "tipo_produto": List<dynamic>.from(tipoProduto.map((x) => x.toMap())),
        "categoria_produto": List<dynamic>.from(categoriaProduto.map((x) => x.toMap())),
        "produto_by_pk": produtoByPk.toMap(),
    };
}

class TipoCategoriaDto {
    TipoCategoriaDto({
        this.id,
        this.descricao,
    });

    final String id;
    final String descricao;

    TipoCategoriaDto copyWith({
        String id,
        String descricao,
    }) => 
        TipoCategoriaDto(
            id: id ?? this.id,
            descricao: descricao ?? this.descricao,
        );

    factory TipoCategoriaDto.fromJson(String str) => TipoCategoriaDto.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory TipoCategoriaDto.fromMap(Map<String, dynamic> json) => TipoCategoriaDto(
        id: json["id"],
        descricao: json["descricao"],
    );

    Map<String, dynamic> toMap() => {
        "id": id,
        "descricao": descricao,
    };
}

class Produto {
    Produto({
        this.id,
        this.nome,
        this.valor,
        this.tipoProduto,
        this.categoriaProduto,
    });

    final String id;
    final String nome;
    final dynamic valor;
    final TipoCategoriaDto tipoProduto;
    final TipoCategoriaDto categoriaProduto;

    Produto copyWith({
        String id,
        String nome,
        dynamic valor,
        TipoCategoriaDto tipoProduto,
        TipoCategoriaDto categoriaProduto,
    }) => 
        Produto(
            id: id ?? this.id,
            nome: nome ?? this.nome,
            valor: valor ?? this.valor,
            tipoProduto: tipoProduto ?? this.tipoProduto,
            categoriaProduto: categoriaProduto ?? this.categoriaProduto,
        );

    factory Produto.fromJson(String str) => Produto.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Produto.fromMap(Map<String, dynamic> json) => Produto(
        id: json["id"],
        nome: json["nome"],
        valor: json["valor"],
        tipoProduto: TipoCategoriaDto.fromMap(json["tipo_produto"]),
        categoriaProduto: TipoCategoriaDto.fromMap(json["categoria_produto"]),
    );

    Map<String, dynamic> toMap() => {
        "id": id,
        "nome": nome,
        "valor": valor,
        "tipo_produto": tipoProduto.toMap(),
        "categoria_produto": categoriaProduto.toMap(),
    };
}
