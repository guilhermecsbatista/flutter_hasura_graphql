import 'package:flutter/material.dart';
import 'package:flutter_hasura_graphql/app/modules/home/pages/update_produto/update_produto_controller.dart';
import 'package:flutter_hasura_graphql/app/shared/widgets/custom_combobox/custom_combobox_widget.dart';
import 'package:flutter_hasura_graphql/app/shared/widgets/label/label_widget.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'models/produto_tipo_categoria_dto.dart';

class UpdateProdutoPage extends StatefulWidget {
  final String id;
  final String title;
  const UpdateProdutoPage({
    Key key,
    this.title = "Atualizar Produto",
    this.id,
  }) : super(key: key);

  @override
  _UpdateProdutoPageState createState() => _UpdateProdutoPageState();
}

class _UpdateProdutoPageState
    extends ModularState<UpdateProdutoPage, UpdateProdutoController> {
  @override
  void initState() {
    super.initState();
    controller.carregarProduto(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Stack(
        children: <Widget>[
          Positioned(
              top: MediaQuery.of(context).size.height - 250,
              right: -50,
              child: CircleAvatar(
                radius: 130,
                backgroundColor: Theme.of(context).primaryColor.withOpacity(.4),
              )),
          Positioned(
              top: MediaQuery.of(context).size.height - 200,
              right: 50,
              child: CircleAvatar(
                radius: 130,
                backgroundColor: Theme.of(context).primaryColor.withOpacity(.2),
              )),
          Positioned(
              top: MediaQuery.of(context).size.height - 150,
              right: 150,
              child: CircleAvatar(
                radius: 130,
                backgroundColor: Theme.of(context).primaryColor.withOpacity(.1),
              )),
          Container(
            height: MediaQuery.of(context).size.height,
            alignment: Alignment.center,
            child: SingleChildScrollView(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  LabelWidget(
                    title: "Descrição",
                  ),
                  Observer(builder: (_) {
                    return TextField(
                      controller: controller.descricaoController,
                      onChanged: controller.setDescricao,
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                      ),
                      decoration: InputDecoration(
                        hintText: "Descrição do Produto",
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor,
                            width: 2,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor,
                            width: 2,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor,
                            width: 2,
                          ),
                        ),
                      ),
                    );
                  }),
                  SizedBox(
                    height: 20,
                  ),
                  LabelWidget(
                    title: "Preço",
                  ),
                  Observer(builder: (_) {
                    return TextField(
                      controller: controller.valorController,
                      onChanged: controller.setValor,
                      keyboardType: TextInputType.number,
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                      ),
                      decoration: InputDecoration(
                        hintText: "Preço",
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor,
                            width: 2,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor,
                            width: 2,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor,
                            width: 2,
                          ),
                        ),
                      ),
                    );
                  }),
                  SizedBox(
                    height: 20,
                  ),
                  LabelWidget(
                    title: "Categoria de Produto",
                  ),
                  Observer(builder: (_) {
                    if (controller.produtoTipoCategoriaDto == null) {
                      return Column(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3),
                              border: Border.all(
                                color: Theme.of(context).primaryColor,
                                width: 2,
                              ),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CircularProgressIndicator(),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                        ],
                      );
                    }
                    return CustomComboboxWidget(
                      items: controller.produtoTipoCategoriaDto.categoriaProduto
                          .map((data) => Model(data.id, data.descricao))
                          .toList(),
                      onChange: (item) {
                        controller.setCategoria(
                          TipoCategoriaDto(
                            id: item.id,
                            descricao: item.descricao,
                          ),
                        );
                      },
                      itemSelecionado: Model(
                        controller.selectedCategoria.id,
                        controller.selectedCategoria.descricao,
                      ),
                    );
                  }),
                  LabelWidget(
                    title: "Tipo do Produto",
                  ),
                  Observer(builder: (_) {
                    if (controller.produtoTipoCategoriaDto == null) {
                      return Column(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3),
                              border: Border.all(
                                color: Theme.of(context).primaryColor,
                                width: 2,
                              ),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CircularProgressIndicator(),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                        ],
                      );
                    }
                    return CustomComboboxWidget(
                      items: controller.produtoTipoCategoriaDto.tipoProduto
                          .map((data) => Model(data.id, data.descricao))
                          .toList(),
                      onChange: (item) {
                        controller.setTipo(
                          TipoCategoriaDto(
                            id: item.id,
                            descricao: item.descricao,
                          ),
                        );
                      },
                      itemSelecionado: Model(
                        controller.selectedTipo.id,
                        controller.selectedTipo.descricao,
                      ),
                    );
                  }),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: RaisedButton(
                      color: Theme.of(context).primaryColor,
                      onPressed: () async {
                        var result = await controller.update(widget.id);

                        if (result) {
                          Navigator.of(context).pop();
                        } else {
                          showDialog(
                            context: context,
                            child: AlertDialog(
                              content:
                                  Text("Erro ao tentar atualizar o produto"),
                              actions: <Widget>[
                                FlatButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text("Fechar"),
                                )
                              ],
                            ),
                          );
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Text(
                          "Atualizar",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
