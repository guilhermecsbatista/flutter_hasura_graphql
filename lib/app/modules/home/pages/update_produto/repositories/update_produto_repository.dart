import 'package:flutter_hasura_graphql/app/modules/home/pages/update_produto/models/produto_tipo_categoria_dto.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:hasura_connect/hasura_connect.dart';

class UpdateProdutoRepository extends Disposable {

  final HasuraConnect _hasuraConnect;

  UpdateProdutoRepository(this._hasuraConnect);

  Future<ProdutoTipoCategoriaDto> getProdutoTipoCategoriaById(String idProduto) async {
    var query = '''
              query getProdutoTipoCategoriaById(\$idProduto: uuid!) {
                tipo_produto {
                  id
                  descricao
                }
                categoria_produto {
                  id
                  descricao
                }
                produto_by_pk(id: \$idProduto) {
                  id
                  nome
                  valor
                  tipo_produto {
                    id
                    descricao
                  }
                  categoria_produto {
                    id
                    descricao
                  }
                }
              }
    ''';

    var snapshot = await _hasuraConnect.query(query, variables: {
      "idProduto": idProduto
    });

    return ProdutoTipoCategoriaDto.fromMap(snapshot["data"]);
  }
  
  Future<bool> updateProduto(
    String idProduto,
    String descricao,
    String valor,
    String categoria,
    String tipo,
  ) async {
    var mutation = '''
        mutation MyMutation(\$idProduto: uuid, \$nome: String, \$valor: float8, \$categoria: uuid, \$tipo: uuid) {
          update_produto(where: {id: {_eq: \$idProduto}}, 
            _set: {nome: \$nome, 
              valor: \$valor, 
              categoria_produto_id: \$categoria, 
              tipo_produto_id: \$tipo}) {
            affected_rows
          }
        }
    ''';

    var snapshot = await _hasuraConnect.mutation(mutation, variables: {
      "idProduto": idProduto,
      "nome": descricao,
      "valor": valor,
      "categoria": categoria,
      "tipo": tipo
    });

    return snapshot["data"]["update_produto"]["affected_rows"] > 0;
  }

  //dispose will be called automatically
  @override
  void dispose() {}
}
