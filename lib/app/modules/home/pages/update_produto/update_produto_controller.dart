import 'package:flutter/widgets.dart';
import 'package:mobx/mobx.dart';

import 'models/produto_tipo_categoria_dto.dart';
import 'repositories/update_produto_repository.dart';

part 'update_produto_controller.g.dart';

class UpdateProdutoController = _UpdateProdutoControllerBase
    with _$UpdateProdutoController;

abstract class _UpdateProdutoControllerBase with Store {
  final UpdateProdutoRepository updateProdutoRepository;

  _UpdateProdutoControllerBase(this.updateProdutoRepository);

  var descricaoController = TextEditingController();
  var valorController = TextEditingController();

  @observable
  String descricao;
  @action
  setDescricao(String _valor) => descricao = _valor;

  @observable
  String valor;
  @action
  setValor(String _valor) => valor = _valor;

  @observable
  TipoCategoriaDto selectedCategoria;
  @action
  setCategoria(TipoCategoriaDto _valor) => selectedCategoria = _valor;

  @observable
  TipoCategoriaDto selectedTipo;
  @action
  setTipo(TipoCategoriaDto _valor) => selectedTipo = _valor;

  @observable
  ProdutoTipoCategoriaDto produtoTipoCategoriaDto;

  @action
  Future<bool> update(String idProduto) async {
    print(descricao);
    print(valor);
    print(selectedCategoria.descricao);
    print(selectedTipo.descricao);

    if (descricao != null &&
        valor != null &&
        selectedCategoria.id != null &&
        selectedTipo.id != null) {
      return await updateProdutoRepository.updateProduto(
        idProduto,
        descricao,
        valor,
        selectedCategoria.id,
        selectedTipo.id,
      );
    }
    return false;
  }

  @action
  void carregarProduto(String idProduto) {
    updateProdutoRepository.getProdutoTipoCategoriaById(idProduto).then((data) {
      produtoTipoCategoriaDto = data;
      descricao = produtoTipoCategoriaDto.produtoByPk.nome;
      descricaoController.text = descricao;
      valor = produtoTipoCategoriaDto.produtoByPk.valor.toString();
      valorController.text = valor;
      selectedCategoria = produtoTipoCategoriaDto.produtoByPk.categoriaProduto;
      selectedTipo = produtoTipoCategoriaDto.produtoByPk.tipoProduto;
    });    
  }
}
