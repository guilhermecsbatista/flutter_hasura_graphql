// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_produto_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$UpdateProdutoController on _UpdateProdutoControllerBase, Store {
  final _$descricaoAtom = Atom(name: '_UpdateProdutoControllerBase.descricao');

  @override
  String get descricao {
    _$descricaoAtom.reportRead();
    return super.descricao;
  }

  @override
  set descricao(String value) {
    _$descricaoAtom.reportWrite(value, super.descricao, () {
      super.descricao = value;
    });
  }

  final _$valorAtom = Atom(name: '_UpdateProdutoControllerBase.valor');

  @override
  String get valor {
    _$valorAtom.reportRead();
    return super.valor;
  }

  @override
  set valor(String value) {
    _$valorAtom.reportWrite(value, super.valor, () {
      super.valor = value;
    });
  }

  final _$selectedCategoriaAtom =
      Atom(name: '_UpdateProdutoControllerBase.selectedCategoria');

  @override
  TipoCategoriaDto get selectedCategoria {
    _$selectedCategoriaAtom.reportRead();
    return super.selectedCategoria;
  }

  @override
  set selectedCategoria(TipoCategoriaDto value) {
    _$selectedCategoriaAtom.reportWrite(value, super.selectedCategoria, () {
      super.selectedCategoria = value;
    });
  }

  final _$selectedTipoAtom =
      Atom(name: '_UpdateProdutoControllerBase.selectedTipo');

  @override
  TipoCategoriaDto get selectedTipo {
    _$selectedTipoAtom.reportRead();
    return super.selectedTipo;
  }

  @override
  set selectedTipo(TipoCategoriaDto value) {
    _$selectedTipoAtom.reportWrite(value, super.selectedTipo, () {
      super.selectedTipo = value;
    });
  }

  final _$produtoTipoCategoriaDtoAtom =
      Atom(name: '_UpdateProdutoControllerBase.produtoTipoCategoriaDto');

  @override
  ProdutoTipoCategoriaDto get produtoTipoCategoriaDto {
    _$produtoTipoCategoriaDtoAtom.reportRead();
    return super.produtoTipoCategoriaDto;
  }

  @override
  set produtoTipoCategoriaDto(ProdutoTipoCategoriaDto value) {
    _$produtoTipoCategoriaDtoAtom
        .reportWrite(value, super.produtoTipoCategoriaDto, () {
      super.produtoTipoCategoriaDto = value;
    });
  }

  final _$updateAsyncAction =
      AsyncAction('_UpdateProdutoControllerBase.update');

  @override
  Future<bool> update(String idProduto) {
    return _$updateAsyncAction.run(() => super.update(idProduto));
  }

  final _$_UpdateProdutoControllerBaseActionController =
      ActionController(name: '_UpdateProdutoControllerBase');

  @override
  dynamic setDescricao(String _valor) {
    final _$actionInfo = _$_UpdateProdutoControllerBaseActionController
        .startAction(name: '_UpdateProdutoControllerBase.setDescricao');
    try {
      return super.setDescricao(_valor);
    } finally {
      _$_UpdateProdutoControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setValor(String _valor) {
    final _$actionInfo = _$_UpdateProdutoControllerBaseActionController
        .startAction(name: '_UpdateProdutoControllerBase.setValor');
    try {
      return super.setValor(_valor);
    } finally {
      _$_UpdateProdutoControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setCategoria(TipoCategoriaDto _valor) {
    final _$actionInfo = _$_UpdateProdutoControllerBaseActionController
        .startAction(name: '_UpdateProdutoControllerBase.setCategoria');
    try {
      return super.setCategoria(_valor);
    } finally {
      _$_UpdateProdutoControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setTipo(TipoCategoriaDto _valor) {
    final _$actionInfo = _$_UpdateProdutoControllerBaseActionController
        .startAction(name: '_UpdateProdutoControllerBase.setTipo');
    try {
      return super.setTipo(_valor);
    } finally {
      _$_UpdateProdutoControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void carregarProduto(String idProduto) {
    final _$actionInfo = _$_UpdateProdutoControllerBaseActionController
        .startAction(name: '_UpdateProdutoControllerBase.carregarProduto');
    try {
      return super.carregarProduto(idProduto);
    } finally {
      _$_UpdateProdutoControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
descricao: ${descricao},
valor: ${valor},
selectedCategoria: ${selectedCategoria},
selectedTipo: ${selectedTipo},
produtoTipoCategoriaDto: ${produtoTipoCategoriaDto}
    ''';
  }
}
