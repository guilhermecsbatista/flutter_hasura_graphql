import 'package:flutter_hasura_graphql/app/modules/home/pages/add_produto_page/models/tipo_categoria_produto_dto.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:hasura_connect/hasura_connect.dart';

class AddProdutoRepository extends Disposable {
  final HasuraConnect _hasuraConnect;

  AddProdutoRepository(this._hasuraConnect);

  Future<TipoCategoriaProdutoDto> getTipoCategoriaProduto() async {
    var query = '''
              query getTipoCategoriaProduto {
                tipo_produto {
                  id,
                  descricao
                }
                categoria_produto {
                  id,
                  descricao
                }  
              }
    ''';

    var snapshot = await _hasuraConnect.query(query);

    return TipoCategoriaProdutoDto.fromMap(snapshot["data"]);
  }

  Future<bool> addProduto(
    String descricao,
    String valor,
    String categoria,
    String tipo,
  ) async {
    var mutation = '''
              mutation MyMutation(\$nome: String, \$valor: float8, \$categoria: uuid, \$tipo: uuid) {
                insert_produto(objects: {nome: \$nome, valor: \$valor, categoria_produto_id: \$categoria, tipo_produto_id: \$tipo}) {
                  affected_rows
                }
              }
    ''';

    var snapshot = await _hasuraConnect.mutation(mutation, variables: {
      "nome": descricao,
      "valor": valor,
      "categoria": categoria,
      "tipo": tipo
    });

    return snapshot["data"]["insert_produto"]["affected_rows"] > 0;
  }

  //dispose will be called automatically
  @override
  void dispose() {}
}
