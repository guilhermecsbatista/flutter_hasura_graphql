// To parse this JSON data, do
//
//     final tipoCategoriaProdutoDto = tipoCategoriaProdutoDtoFromMap(jsonString);

import 'dart:convert';

class TipoCategoriaProdutoDto {
  TipoCategoriaProdutoDto({
    this.tipoProduto,
    this.categoriaProduto,
  });

  final List<TipoCategoriaDto> tipoProduto;
  final List<TipoCategoriaDto> categoriaProduto;

  TipoCategoriaProdutoDto copyWith({
    List<TipoCategoriaDto> tipoProduto,
    List<TipoCategoriaDto> categoriaProduto,
  }) =>
      TipoCategoriaProdutoDto(
        tipoProduto: tipoProduto ?? this.tipoProduto,
        categoriaProduto: categoriaProduto ?? this.categoriaProduto,
      );

  factory TipoCategoriaProdutoDto.fromJson(String str) =>
      TipoCategoriaProdutoDto.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory TipoCategoriaProdutoDto.fromMap(Map<String, dynamic> json) =>
      TipoCategoriaProdutoDto(
        tipoProduto: List<TipoCategoriaDto>.from(
            json["tipo_produto"].map((x) => TipoCategoriaDto.fromMap(x))),
        categoriaProduto: List<TipoCategoriaDto>.from(
            json["categoria_produto"].map((x) => TipoCategoriaDto.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "tipo_produto": List<dynamic>.from(tipoProduto.map((x) => x.toMap())),
        "categoria_produto":
            List<dynamic>.from(categoriaProduto.map((x) => x.toMap())),
      };
  
  static List<TipoCategoriaProdutoDto> fromJsonList(List list) {
    if (list == null) return null;
    return list
        .map<TipoCategoriaProdutoDto>((item) => TipoCategoriaProdutoDto.fromMap(item))
        .toList();
  }
}

class TipoCategoriaDto {
  TipoCategoriaDto({
    this.id,
    this.descricao,
  });

  final String id;
  final String descricao;

  TipoCategoriaDto copyWith({
    String id,
    String descricao,
  }) =>
      TipoCategoriaDto(
        id: id ?? this.id,
        descricao: descricao ?? this.descricao,
      );

  factory TipoCategoriaDto.fromJson(String str) => TipoCategoriaDto.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory TipoCategoriaDto.fromMap(Map<String, dynamic> json) => TipoCategoriaDto(
        id: json["id"],
        descricao: json["descricao"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "descricao": descricao,
      };
}
