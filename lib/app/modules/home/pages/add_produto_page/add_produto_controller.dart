import 'package:flutter_hasura_graphql/app/modules/home/pages/add_produto_page/repositories/add_produto_repository.dart';
import 'package:mobx/mobx.dart';

import 'models/tipo_categoria_produto_dto.dart';

part 'add_produto_controller.g.dart';

class AddProdutoController = _AddProdutoControllerBase
    with _$AddProdutoController;

abstract class _AddProdutoControllerBase with Store {
  final AddProdutoRepository addProdutoRepository;

  _AddProdutoControllerBase(this.addProdutoRepository) {
    addProdutoRepository.getTipoCategoriaProduto().then((data) {
      tipoCategoriaProdutoDto = data;
    });
  }

  @observable
  String descricao;
  @action
  setDescricao(String _valor) => descricao = _valor;

  @observable
  String valor;
  @action
  setValor(String _valor) => valor = _valor;

  @observable
  TipoCategoriaDto selectedCategoria;
  @action
  setCategoria(TipoCategoriaDto _valor) => selectedCategoria = _valor;

  @observable
  TipoCategoriaDto selectedTipo;
  @action
  setTipo(TipoCategoriaDto _valor) => selectedTipo = _valor;

  @observable
  TipoCategoriaProdutoDto tipoCategoriaProdutoDto;

  @action
  Future<bool> salvar() async {
    print(descricao);
    print(valor);
    print(selectedCategoria.descricao);
    print(selectedTipo.descricao);

    if (descricao != null &&
        valor != null &&
        selectedCategoria.id != null &&
        selectedTipo.id != null) {
      return await addProdutoRepository.addProduto(
          descricao, valor, selectedCategoria.id, selectedTipo.id);
    }
    return false;
  }
}
