import 'package:flutter/material.dart';
import 'package:flutter_hasura_graphql/app/modules/home/pages/add_produto_page/add_produto_controller.dart';
import 'package:flutter_hasura_graphql/app/shared/widgets/custom_combobox/custom_combobox_widget.dart';
import 'package:flutter_hasura_graphql/app/shared/widgets/label/label_widget.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'models/tipo_categoria_produto_dto.dart';

class AddProdutoPage extends StatefulWidget {
  final String title;
  const AddProdutoPage({Key key, this.title = "AddProduto"})
      : super(key: key);

  @override
  _AddProdutoPageState createState() => _AddProdutoPageState();
}

class _AddProdutoPageState extends ModularState<AddProdutoPage, AddProdutoController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Stack(
        children: <Widget>[
          Positioned(
              top: MediaQuery.of(context).size.height - 250,
              right: -50,
              child: CircleAvatar(
                radius: 130,
                backgroundColor: Theme.of(context).primaryColor.withOpacity(.4),
              )),
          Positioned(
              top: MediaQuery.of(context).size.height - 200,
              right: 50,
              child: CircleAvatar(
                radius: 130,
                backgroundColor: Theme.of(context).primaryColor.withOpacity(.2),
              )),
          Positioned(
              top: MediaQuery.of(context).size.height - 150,
              right: 150,
              child: CircleAvatar(
                radius: 130,
                backgroundColor: Theme.of(context).primaryColor.withOpacity(.1),
              )),
          Container(
            height: MediaQuery.of(context).size.height,
            alignment: Alignment.center,
            child: SingleChildScrollView(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  LabelWidget(
                    title: "Descrição",
                  ),
                  TextField(
                    onChanged: controller.setDescricao,
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                    ),
                    decoration: InputDecoration(
                      hintText: "Descrição do Produto",
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Theme.of(context).primaryColor,
                          width: 2,
                        ),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Theme.of(context).primaryColor,
                          width: 2,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Theme.of(context).primaryColor,
                          width: 2,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  LabelWidget(
                    title: "Valor",
                  ),
                  TextField(
                    onChanged: controller.setValor,
                    keyboardType: TextInputType.number,
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                    ),
                    decoration: InputDecoration(
                      hintText: "Valor",
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Theme.of(context).primaryColor,
                          width: 2,
                        ),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Theme.of(context).primaryColor,
                          width: 2,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Theme.of(context).primaryColor,
                          width: 2,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  LabelWidget(
                    title: "Categoria de Produto",
                  ),
                  Observer(builder: (_) {
                    if (controller.tipoCategoriaProdutoDto == null) {
                      return Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(3),
                          border: Border.all(
                            color: Theme.of(context).primaryColor,
                            width: 2,
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: CircularProgressIndicator(),
                            ),
                          ],
                        ),
                      );
                    }
                    return CustomComboboxWidget(
                      items: controller.tipoCategoriaProdutoDto.categoriaProduto
                          .map((data) => Model(data.id, data.descricao))
                          .toList(),
                      onChange: (item) {
                        controller.setCategoria(
                          TipoCategoriaDto(
                            id: item.id,
                            descricao: item.descricao,
                          ),
                        );
                      },
                      itemSelecionado: null,
                    );
                  }),
                  SizedBox(
                    height: 20,
                  ),
                  LabelWidget(
                    title: "Tipo Produto",
                  ),
                  Observer(builder: (_) {
                    if (controller.tipoCategoriaProdutoDto == null) {
                      return Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(3),
                          border: Border.all(
                            color: Theme.of(context).primaryColor,
                            width: 2,
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: CircularProgressIndicator(),
                            ),
                          ],
                        ),
                      );
                    }
                    return CustomComboboxWidget(
                      items: controller.tipoCategoriaProdutoDto.tipoProduto
                          .map((data) => Model(data.id, data.descricao))
                          .toList(),
                      onChange: (item) {
                        controller.setTipo(
                          TipoCategoriaDto(
                            id: item.id,
                            descricao: item.descricao,
                          ),
                        );
                      },
                      itemSelecionado: null,
                    );
                  }),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: RaisedButton(
                      color: Theme.of(context).primaryColor,
                      onPressed: () async {
                        var result = await controller.salvar();

                        if (result) {
                          Navigator.of(context).pop();
                        } else {
                          showDialog(
                            context: context,
                            child: AlertDialog(
                              content: Text("Erro ao tentar salvar o produto"),
                              actions: <Widget>[
                                FlatButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text("Fechar"),
                                )
                              ],
                            ),
                          );
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Text(
                          "Salvar",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
