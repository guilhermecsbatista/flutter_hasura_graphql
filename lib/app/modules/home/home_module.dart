import 'package:flutter_hasura_graphql/app/modules/home/pages/update_produto/repositories/update_produto_repository.dart';
import 'package:flutter_hasura_graphql/app/modules/home/pages/update_produto/update_produto_controller.dart';
import 'package:flutter_hasura_graphql/app/modules/home/pages/add_produto_page/add_produto_controller.dart';
import 'package:flutter_hasura_graphql/app/modules/home/pages/add_produto_page/add_produto_page.dart';
import 'package:flutter_hasura_graphql/app/modules/home/pages/update_produto/update_produto_page.dart';
import 'package:flutter_hasura_graphql/app/modules/home/repositories/home_repository.dart';
import 'package:flutter_hasura_graphql/app/modules/home/widgets/card_produto/card_produto_controller.dart';
import 'package:flutter_hasura_graphql/app/modules/home/home_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_hasura_graphql/app/modules/home/home_page.dart';
import 'package:hasura_connect/hasura_connect.dart';

import '../../app_module.dart';
import 'pages/add_produto_page/repositories/add_produto_repository.dart';

class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => UpdateProdutoController(i.get<UpdateProdutoRepository>())),
        Bind((i) => AddProdutoController(i.get<AddProdutoRepository>())),
        //Controllers
        Bind((i) => CardProdutoController()),
        Bind((i) => HomeController(i.get<HomeRepository>())),
        //Repositories
        Bind((i) => HomeRepository(AppModule.to.get<HasuraConnect>())),
        Bind((i) => AddProdutoRepository(AppModule.to.get<HasuraConnect>())),
        Bind((i) => UpdateProdutoRepository(AppModule.to.get<HasuraConnect>())),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => HomePage()),
        Router('/AddProduto', child: (_, args) => AddProdutoPage()),
        Router('/UpdateProduto/:id',
            child: (_, args) => UpdateProdutoPage(
                  id: args.params['id'],
                )),
      ];

  static Inject get to => Inject<HomeModule>.of();
}
