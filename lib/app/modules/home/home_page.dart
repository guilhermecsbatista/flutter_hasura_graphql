import 'package:flutter/material.dart';
import 'package:flutter_hasura_graphql/app/modules/home/widgets/card_produto/card_produto_widget.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'home_controller.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Observer(builder: (BuildContext context) {
        if (controller.listaProdutos.hasError) {
          return Center(
            child: Text("Ocorreu um erro ao carregar a lista de produtos."),
          );
        }

        if (controller.listaProdutos.value == null) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        controller.listaProdutos.value.sort((value1, value2) => value1.nome.toUpperCase().compareTo(value2.nome.toUpperCase()));

        return ListView.builder(
          itemCount: controller.listaProdutos.value.length,
          itemBuilder: (BuildContext context, int index) {
            return CardProdutoWidget(
              id: controller.listaProdutos.value[index].id,
              nomeProduto: controller.listaProdutos.value[index].nome,
              valor: controller.listaProdutos.value[index].valor.toString(),
              categoriaProduto: controller
                  .listaProdutos.value[index].categoriaProduto.descricao,
              tipoProduto:
                  controller.listaProdutos.value[index].tipoProduto.descricao,
            );
          },
        );
      }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, "/AddProduto");
        },
        child: Icon(Icons.add, color: Colors.white),
      ),
    );
  }
}
